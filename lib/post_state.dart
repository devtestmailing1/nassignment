import 'package:equatable/equatable.dart';
import 'package:flutterassignment/post.dart';

abstract class PostState extends Equatable{
  PostState([List props = const[]]): super (props);
}

class PostUninitialize extends PostState{
  @override
  String toString() => 'PostUninitialized';
}

class PostError extends PostState{
  @override
  String toString() => 'PostError';
}

class PostLoaded extends PostState{
  final List<Post>posts;
  final bool hasReachedMax;

  PostLoaded({this.posts, this.hasReachedMax,}):super([posts,hasReachedMax]);

  PostLoaded copyWith({List<Post> posts, bool hasReachedMax,}){
    return PostLoaded(
      posts: posts??this.posts,
      hasReachedMax: hasReachedMax?? this.hasReachedMax,
    );
  }

  @override
  String toString() => 'PostLoaded{post:${posts.length},hasReachedMax:$hasReachedMax}';
}

