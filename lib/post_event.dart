import 'package:equatable/equatable.dart';
import 'package:flutterassignment/post.dart';

abstract class PostEvent extends Equatable{}

class Fetch extends PostEvent{

  @override
  String toString() => 'Fetch';

}