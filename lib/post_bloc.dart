import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutterassignment/post.dart';
import 'package:http/http.dart' as http;
import 'package:flutterassignment/post_event.dart';
import 'package:flutterassignment/post_state.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  final http.Client httpClient;

  PostBloc({@required this.httpClient});

  @override
  PostState get initialState => PostUninitialize();


  @override
  Stream<PostState> transform(
      Stream<PostEvent> events,
      Stream<PostState> Function(PostEvent event) next,
      ) {
    return super.transform(
      (events as Observable<PostEvent>).debounceTime(
        Duration(milliseconds: 500),
      ),
      next,
    );
  }

  @override
  Stream<PostState> mapEventToState(
    PostEvent event,
  ) async* {
    final currentStat = currentState;
    if (event is Fetch && !_hasReachedMax(currentStat)) {
      try {
        if (currentStat is PostUninitialize) {
          final posts = await _fetchPosts(0, 20);
          print('lenght>>> ${posts.length}');
          yield PostLoaded(posts: posts, hasReachedMax: false);
        }
        if (currentStat is PostLoaded) {
          final posts = await _fetchPosts(currentStat.props.length, 20);
          print('lenght>>> ${posts.length}');
          yield posts.isEmpty
              ? currentStat.copyWith(hasReachedMax: true)
              : PostLoaded(
                  posts: currentStat.props + posts, hasReachedMax: false);
        }
      } catch (_) {
        yield PostError();
      }
    }
  }

  bool _hasReachedMax(PostState state) =>
      state is PostLoaded && state.hasReachedMax;

  Future<List<Post>> _fetchPosts(int startIndex, int limit) async {
    final response = await httpClient.get(
       'http://jsonplaceholder.typicode.com/todos?_start=$startIndex&_limit=$limit');
        //'http://jsonplaceholder.typicode.com/todos?_start=0&_limit=20');
    if (response.statusCode == 200) {
      print('hello ladies');
      final data = json.decode(response.body) as List;
      return data.map((rawPost) {
        print('hello ladies2');
        return Post(
          id: rawPost['id'],
          title: rawPost['title'],
          completed: rawPost['completed'],
        );
      }).toList();
    } else {
      throw Exception('error fetching posts');
    }
  }
}
